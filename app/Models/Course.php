<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'body', 'image', 'price'
    ];

    public function episodes()
    {
        return $this->hasMany(Episod::class);
    }

}
