<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    public function upload(Request $request, Filesystem $filesystem)
    {
        $request->validate([
            'image' => ['required', 'mimes:png,jpeg,bmp', 'max:10240']
        ]);
        $file = $request->file('image');

        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;

        $dirUpload = "/upload/image/$year/$month/$day";
        $fileName = trim($file->getClientOriginalName());


        if ($filesystem->exists(public_path($dirUpload . '/' . $fileName))) {
            $fileName = Carbon::now()->timestamp. '-' .$fileName  ;
        }

        $file->move(public_path($dirUpload), $fileName);

        return response()->json([
            'data' => url($dirUpload . '/' . $fileName),
            'status' => 'success'
        ], 200);

    }
}
