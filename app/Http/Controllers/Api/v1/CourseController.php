<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Http\Resources\Api\v1\CourseCollection;
use App\Models\Course;
use Illuminate\Http\Request;
use \App\Http\Resources\Api\v1\Course as CourseSingle;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index()
    {
        $course = Course::all();
        return new CourseCollection($course);
    }

    public function single(Course $course)
    {
        return new CourseSingle($course);
    }

    public function store(Request $request)
    {

        $validate = Validator::make((array)$request, [
            'title' => ['required'],
            'body' => ['required']
        ]);
        if ($validate->fails()){
            return response()->json([
                'data' => 'error'
            ]);
        }
        return response()->json([
            'data' => $request->all()
        ]);
    }

}
