<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|exists:users',
            'password' => 'required'
        ]);

        if (!Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return response()->json([
                'msg' => 'نام کاربری یا پسورد اشتباه است',
                'data' => [],
                'status' => 'error'
            ], 404);
        }

        auth()->user()->tokens()->delete();

        $token = $request->user()->createToken(\auth()->user()->name)->plainTextToken;

        return response()->json([
            'msg' => 'خوش آمدید',
            'data' => new \App\Http\Resources\Api\v1\Auth(auth()->user()),
            'token' => $token,
            'status' => 'ok'
        ], 200);

    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $token = $user->createToken($user->name)->plainTextToken;

        return response()->json([
            'msg' => 'ثبت شد',
            'data' => new \App\Http\Resources\Api\v1\Auth($user),
            'token' => $token,
            'status' => 'ok'
        ], 200);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'msg' => 'خارج شدین',
            'status' => 'ok'
        ], 200);

    }
}
