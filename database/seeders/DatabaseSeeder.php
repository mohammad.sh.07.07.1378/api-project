<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Episod;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::factory()->create();
        Course::factory(5)->create(['user_id' => $user])->each(function ($course) {
            Episod::factory(rand(5, 20))->make()->each(function ($episode, $key) use ($course) {
                $episode->number = $key + 1;
                $course->episodes()->save($episode);
            });
        });
    }
}
